<h1>Financial Transaction</h1>

<h2>Running project</h2>
<p>This is an springboot project so, you can run it with:</p>
Linux: ./gradlew bootRun
<br>
Windows: .\gradlew.bat bootRun

<h2>Running compiled project</h2>
The .jar is already uploaded and compile on build/libs/financialTransactions-1.0.0.jar
<br>
 so you can run it with:
 <br>
 java -jar financialTransactions-1.0.0.jar
 
<h2>Testing</h2>
It has swagger intalled, so you can run some tests using the UI on:
<br>
http://localhost:8080/swagger-ui.htm 
