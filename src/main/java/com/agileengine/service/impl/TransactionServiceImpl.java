package com.agileengine.service.impl;

import com.agileengine.dao.TransactionDao;
import com.agileengine.domain.Transaction;
import com.agileengine.domain.TransactionType;
import com.agileengine.service.TransactionService;
import org.apache.logging.log4j.util.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;
import java.util.NoSuchElementException;

@Service
public class TransactionServiceImpl implements TransactionService {

    @Autowired
    private TransactionDao transactionDao;

    @Override
    public void addTransaction(String type, BigDecimal amount) {
        // first make a validation and then create the transaction
        validateTransaction(type, amount);
        // new transaction to be saved
        Transaction transaction = new Transaction(type, amount);

        transactionDao.save(transaction);
    }

    @Override
    public List<Transaction> getall() {
        return transactionDao.findAll();
    }

    private void validateTransaction(String type, BigDecimal amount) {
        // validate Type, it must be Credit or debit
        validateTransactionType(type);

        // validate getting an amount
        if(amount == null){
            throw new IllegalArgumentException("Amount is required");
        }

        // validate transaction leading to negative amount
        validateTransactionFinalAmount(type, amount);
    }

    private void validateTransactionFinalAmount(String type, BigDecimal amount) {
        // Get all the transaction
        List<Transaction> transactions = transactionDao.findAll();

        // Init the new value with the amount to add
        BigDecimal newTotal = amount.multiply(TransactionType.multiplier(type));
        BigDecimal amountWithSign;

        // Add positive or negative values
        for (Transaction transaction: transactions) {
            amountWithSign = transaction.getAmount().multiply(TransactionType.multiplier(transaction.getType()));
            newTotal = newTotal.add(amountWithSign);
        }

        // Do the validation
        if(BigDecimal.ZERO.compareTo(newTotal) > 0 ){
            throw new IllegalArgumentException("Invalid transaction. This transaction leads to negative amount");
        }
    }

    private void validateTransactionType(String type) {
        if (Strings.isNotEmpty(type)) {
            try {
                TransactionType.getType(type);
            } catch (NoSuchElementException e) {
                // If i get this exception its means that is an invalid type
                throw new IllegalArgumentException("Invalid transaction type");
            }
        } else {
            throw new IllegalArgumentException("Type is required");
        }
    }
}
