package com.agileengine.service;

import com.agileengine.domain.Transaction;

import java.math.BigDecimal;
import java.util.List;

public interface TransactionService {
    void addTransaction(String type, BigDecimal amount);

    List<Transaction> getall();
}
