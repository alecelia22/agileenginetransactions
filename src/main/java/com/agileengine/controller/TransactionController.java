package com.agileengine.controller;

import com.agileengine.domain.Transaction;
import com.agileengine.dto.TransactionDTO;
import com.agileengine.service.TransactionService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@RestController
@RequestMapping("/api/transaction")
public class TransactionController {

    Logger logger = LoggerFactory.getLogger(TransactionController.class);

    @Autowired
    private TransactionService transactionService;

    @PostMapping
    public ResponseEntity<String> financialTransaction(@RequestBody TransactionDTO transactionDTO) {
        try {
            transactionService.addTransaction(transactionDTO.getType(), transactionDTO.getAmount());
            logger.info(String.format("Added new transaction of type %s with value %s", transactionDTO.getType(), transactionDTO.getAmount()));
            return ResponseEntity.ok("Transaction added sucessfully");
        } catch (IllegalArgumentException e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        } catch (Exception e) {
            logger.error(String.format("Unexpecting error adding transaction of type %s with value %s ",
                    transactionDTO.getType(), transactionDTO.getAmount()), e);
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, e.getMessage());
        }
    }

    @GetMapping
    public ResponseEntity<List<Transaction>> getAll() {
        try {
            return ResponseEntity.ok(transactionService.getall());
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, e.getMessage());
        }
    }
}
