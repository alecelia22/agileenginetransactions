package com.agileengine.domain;

import java.math.BigDecimal;
import java.util.Arrays;

public enum TransactionType {
    CREDIT,
    DEBIT;

    public static TransactionType getType(String transactionType){
        return Arrays.stream(TransactionType.values())
                .filter(type -> type.name().equalsIgnoreCase(transactionType))
                .findFirst().get();
    }

    public static BigDecimal multiplier(String type) {
        return CREDIT.name().equalsIgnoreCase(type) ? BigDecimal.ONE : new BigDecimal(-1);
    }
}
