package com.agileengine.dao;

import com.agileengine.domain.Transaction;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Lock;
import org.springframework.stereotype.Repository;

import javax.persistence.LockModeType;

@Repository
public interface TransactionDao extends JpaRepository<Transaction, String> {

    @Override
    @Lock(LockModeType.PESSIMISTIC_WRITE)
    Transaction save(Transaction entity);
}
