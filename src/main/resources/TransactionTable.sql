CREATE TABLE IF NOT EXISTS transaction (
   id VARCHAR(11) not null,
   type VARCHAR(10),
   amount numeric,
   effective_date date,
   account_id int(11);
);